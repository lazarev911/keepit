/**
 * Determine of a single ACL entry is "dominated" by the users effective ACL. 
 * One ACL dominates another, if it contains at least the same access rights.
 *
 * @param {string}  concrete    Single ACL entry
 * @param {string}  effective   List of ACL entries
 * @returns {boolean}
 */
function acl_concrete_dominated(concrete, effective) {
    if (typeof concrete !== 'string') {
        throw new Error('Concrete is not a string');
    }

    if (typeof effective !== 'string') {
        throw new Error('Effective is not a string');
    }

    const effectiveList = effective.split(':').reduce((acc, cur) => {
        const item = cur.split('/');
        acc[item[1]] = item[0];
        return acc;
    }, {});

    const concreteObj = concrete.split('/');
    const currentOperations = effectiveList[concreteObj[1]];

    if (!concreteObj[0]) {
        throw new Error('No operations at concrete');
    }

    if (!currentOperations) {
        // Also it can be return false
        throw new Error('No operations at effective for concrete');
    }

    return concreteObj[0].split('').reduce((acc, cur) => {
        return (acc !== false && currentOperations.indexOf(cur) !== -1);
    }, true);
}

console.log(acl_concrete_dominated('P/Time', 'GPU/Users:G/Time'));      // false
console.log(acl_concrete_dominated('GH/Time', 'GPU/Users:G/Time'));     // false
console.log(acl_concrete_dominated('GH/Time', 'GPU/Users:GH/Time'));    // true
console.log(acl_concrete_dominated('GH/Time', 'GPU/Users:GHP/Time'));   // true
console.log(acl_concrete_dominated('HG/Time', 'GPU/Users:GHP/Time'));   // true
console.log(acl_concrete_dominated('HG/Time', 'GHP/Time'));             // true

// Errors
acl_concrete_dominated({}, 'GPU/Users:GHP/Time');                       // throw Error: Concrete is not a string
acl_concrete_dominated('GH/Time', {});                                  // throw Error: Effective is not a string
acl_concrete_dominated('GH/Time');                                      // throw Error: Effective is not a string
acl_concrete_dominated('GH/', 'GPU/Users:GHP/Time');                    // throw Error: No operations at effective for concrete
acl_concrete_dominated('GH/AnotherTime', 'GPU/Users:GHP/Time');         // throw Error: No operations at effective for concrete
acl_concrete_dominated('/Time', 'GPU/Users:GHP/Time');                  // throw Error: No operations at concrete
acl_concrete_dominated('HG/Time', 'GPU/Users:GHP');                     // throw Error: No operations at effective for concrete
acl_concrete_dominated('HG/Time', 'GPU/UsersGHP/Users');                // throw Error: No operations at effective for concrete
acl_concrete_dominated('GH/Time', 'GPU/Users:GHP/AnotherTime');         // throw Error: No operations at effective for concrete
